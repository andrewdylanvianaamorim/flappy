'use strict';

let canvas = document.getElementById("canvas").getContext('2d');

let xJogador = 50
let yJogador = canvas.canvas.height / 2;
let velJogador = 0;
let acelJogador = 0;
let tamanhoJogador = 20;

let estaPrecionandoEspaco = false;
let impulso = 0;
let cooldownEspaco = 0;

let delta = 0;
let fimFrame = 0;


let velImpulso = 350 * 4;

const GRAVIDADE = 350;

window.addEventListener("keydown", (e) => {
	if (e.code == "Space") {
		if (!estaPrecionandoEspaco) {
			estaPrecionandoEspaco = true;
			cooldownEspaco = Date.now();
		}
	}		
});

window.addEventListener("keyup", (e) => {
	if (e.code == "Space") {
		estaPrecionandoEspaco = false;
		cooldownEspaco = 0;
	}	
});



setInterval(() => {

	let delta = (fimFrame == 0) ? 0 : (Date.now() - fimFrame) / 1000.0;

	if (Date.now() - cooldownEspaco < 50 && estaPrecionandoEspaco) {
		impulso += velImpulso;
	} else if (Date.now() - cooldownEspaco > 50) {
		cooldownEspaco = 0;
		impulso = 0;
	}

	acelJogador = GRAVIDADE - impulso;

	velJogador = velJogador + acelJogador * delta;

	console.log(velJogador);
	
	yJogador = yJogador + velJogador * delta + (acelJogador * (delta*delta) / 2);
		

	//começo do frame
	canvas.fillStyle = "blue";
	canvas.fillRect(0,0,canvas.canvas.width, canvas.canvas.height);


	//desenhando jogador
	canvas.fillStyle = "red";
	canvas.fillRect(xJogador, yJogador, tamanhoJogador, tamanhoJogador);
	

	
	fimFrame = Date.now();
}, 1/60);
